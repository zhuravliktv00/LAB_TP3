package Clients;

import Converter.FileConverter;
import Entities.File;

import java.io.*;
import java.net.Socket;
import java.util.stream.Stream;

public class Client1 {
    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 8082);
                while(true) {
                    reader = new BufferedReader(new InputStreamReader(System.in));
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                    System.out.println("Введите полный адрес файла");
                    String fileLocation = reader.readLine();
                    if ("exit".equals(fileLocation)) {
                        break;
                    }
                    if (!fileLocation.isEmpty()) {
                        InputStream inStream = new FileInputStream(fileLocation);
                        byte[] bytes = inStream.readAllBytes();
                        File file = new File();
                        file.setBytes(bytes);
                        file.setFormat(getFormat(fileLocation));
                        String json = FileConverter.toJSON(file);

                        out.write(json + "\n");
                        System.out.println("Был послан файл: " + fileLocation);
                        out.flush();
                        inStream.close();
                    }
                    String serverJson = in.readLine();
                    File fileFromServer = FileConverter.toJavaObject(serverJson);
                    String fileName = Integer.toString((int) (Math.random() * (100001)));

                    System.out.println("Файл получил имя: " + fileName);

                    OutputStream outStream = new FileOutputStream("C:\\Users\\Zver\\Documents\\ТП\\out\\" +
                            fileName + "." +
                            fileFromServer.getFormat());
                    outStream.write(fileFromServer.getBytes());
                    System.out.println("Полный путь до файла: C:\\Users\\Zver\\Documents\\ТП\\out\\" + fileName + "." + fileFromServer.getFormat());
                    outStream.close();
                }
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
    private static String getFormat(String fileLocation) {
        return fileLocation.substring(fileLocation.lastIndexOf(".") + 1);
    }
}
