package Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class MainServer {
    public static final int PORT = 8082;

    public static LinkedList<Server> serverList = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        System.out.println("Ждём подключений");
        ServerSocket server = new ServerSocket(PORT);
        try {
            while (true) {
                Socket socket = server.accept();
                try {
                    serverList.add(new Server(socket));

                    System.out.println("Подключился пользователь");
                    System.out.println("Лист подключений: "+ serverList);
                } catch (IOException e) {
                    System.err.println(e);
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}
