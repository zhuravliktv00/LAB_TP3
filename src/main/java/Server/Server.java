package Server;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;

public class Server extends Thread {
    private Socket socket;

    private BufferedReader in;

    private BufferedWriter out;

    public Server(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    @Override
    public void run() {
        String json;
        try {
            while (true) {
                json = in.readLine();
                if (json.equals("exit")) {
                    break;
                }
                for (Server vr : MainServer.serverList) {
                    if (vr.socket.equals(this.socket)) {
                        continue;
                    }
                    vr.send(json);
                }
                ObjectMapper mapper = new ObjectMapper();
                File file = new File("C:\\Users\\Zver\\Documents\\ТП\\out\\countFiles.json");
                if (file.exists()) {
                    CountFiles countFilesObject = mapper.readValue(file, CountFiles.class);
                    countFilesObject.setCountFiles(countFilesObject.getCountFiles() + 1);
                    mapper.writeValue(file, countFilesObject);
                } else {
                    CountFiles countFilesObject = new CountFiles();
                    countFilesObject.setCountFiles(1);
                    mapper.writeValue(file, countFilesObject);
                }
            }

        } catch (IOException e) {
            System.err.println(e);
        }
    }

    private void send(String json) {
        try {
            out.write(json + "\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }
}
