package Converter;


import Entities.File;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class FileConverter {

    static ObjectMapper  mapper = new ObjectMapper();

    public static String toJSON(File file) throws IOException {
        System.out.println(mapper.writeValueAsString(file));
        return mapper.writeValueAsString(file);
    }

    public static File toJavaObject(String json) throws IOException {
        return mapper.readValue(json, File.class);
    }
}
